// Binary ci programmatically configures CI settings for all go-team
// packaging repositories.
package main

import (
	"flag"
	"log"
	"os"

	"salsa.debian.org/go-team/ci/config"

	gitlab "github.com/xanzy/go-gitlab"
)

const group = "go-team/packages"

var salsa = salsaClient()

func salsaClient() *gitlab.Client {
	cl := gitlab.NewClient(nil, os.Getenv("SALSA_TOKEN"))
	cl.SetBaseURL("https://salsa.debian.org/api/v4")
	return cl
}

func listProjects() ([]*gitlab.Project, error) {
	var projects []*gitlab.Project
	page := 1
	for {
		partial, resp, err := salsa.Groups.ListGroupProjects(group, &gitlab.ListGroupProjectsOptions{Page: page})
		if err != nil {
			return nil, err
		}
		projects = append(projects, partial...)
		if page == resp.TotalPages {
			break
		}
		page = resp.NextPage
	}

	return projects, nil
}

func logic() error {
	projects, err := listProjects()
	if err != nil {
		return err
	}

	// For testing:
	// project, _, err := salsa.Projects.GetProject(7734)
	// if err != nil {
	// 	return err
	// }
	// projects := []*gitlab.Project{project}

	log.Printf("found %d projects", len(projects))
	for _, p := range projects {
		if err := config.All(p); err != nil {
			return err
		}
	}

	return nil
}

func main() {
	flag.Parse()
	if err := logic(); err != nil {
		log.Fatal(err)
	}
}
