package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"sort"
)

// TODO: move this type into an internal package so that it can be shared between ci-{build,diff}
type result struct {
	Out string `json:"output"`
	Err string `json:"error"`
}

func load(path string) (map[string]result, error) {
	result := make(map[string]result)
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(b, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func sorted(r map[string]result) []string {
	keys := make([]string, 0, len(r))
	for key := range r {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	return keys
}

func diff(beforePath, afterPath string) error {
	exitCode := 0

	before, err := load(beforePath)
	if err != nil {
		return err
	}
	after, err := load(afterPath)
	if err != nil {
		return err
	}

	for _, importPath := range sorted(before) {
		if _, ok := after[importPath]; !ok {
			// TODO: what bearing should this have on the exit code?
			fmt.Printf("no longer attempted to compile? %s\n", importPath)
			continue
		}
	}

	for _, importPath := range sorted(after) {
		a := after[importPath]
		b, ok := before[importPath]
		if !ok {
			if a.Err == "" {
				fmt.Printf("new package: %s\n", importPath)
				continue
			}
			fmt.Printf("new package: %s. error %s\n", importPath, a.Err)
			fmt.Println(a.Out)
			exitCode = 1
			continue
		}
		// Compiles before and after:
		if b.Err == "" && a.Err == "" {
			continue
		}
		// Did not compile before, does compile after:
		if b.Err != "" && a.Err == "" {
			fmt.Printf("now compiles: %s\n", importPath)
			continue
		}
		// Compiled before, does not compile after:
		if b.Err == "" && a.Err != "" {
			fmt.Printf("no longer compiles: %s. error %s\n", importPath, a.Err)
			fmt.Println(a.Out)
			exitCode = 1
			continue
		}
		// Does not compile before nor after:
		if a.Err != b.Err {
			// does not compile with a different error
			fmt.Printf("still does not compile (but different error): %s. error %q (before: %q)\n", importPath, a.Err, b.Err)
			fmt.Println(a.Out)
			exitCode = 1
		}
	}
	os.Exit(exitCode)
	return nil
}

func main() {
	flag.Parse()
	if flag.NArg() < 2 {
		log.Fatal("syntax: ci-build <before> <after>")
	}
	if err := diff(flag.Arg(0), flag.Arg(1)); err != nil {
		log.Fatal(err)
	}
}
