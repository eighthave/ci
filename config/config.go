// Package config contains functions to configure GitLab projects for the Debian
// go-team.
package config

import (
	"log"
	"net/url"
	"os"
	"strings"

	gitlab "github.com/xanzy/go-gitlab"
)

// TODO(later): update dh-make-golang to be consistent with this code

const (
	runnerID   = 8 // chuchi
	ciFileName = "debian/gitlab-ci.yml"
)

// TODO(later): add “[ci skip]” to the commit message when we don’t want to
// trigger builds in all repos
const commitMsg = `update debian/gitlab-ci.yml (using salsa.debian.org/go-team/ci/cmd/ci)

Gbp-Dch: Ignore`

var salsa = salsaClient()

func salsaClient() *gitlab.Client {
	cl := gitlab.NewClient(nil, os.Getenv("SALSA_TOKEN"))
	cl.SetBaseURL("https://salsa.debian.org/api/v4")
	return cl
}

func All(p *gitlab.Project) error {
	if err := FixSettings(p); err != nil {
		return err
	}
	if err := FixProjectRunners(p); err != nil {
		return err
	}
	if err := WriteGitlabCiYml(p); err != nil {
		return err
	}
	if err := FixWebhooks(p); err != nil {
		return err
	}
	if err := ProtectBranches(p); err != nil {
		return err
	}
	return nil
}

func FixSettings(p *gitlab.Project) error {
	change := false
	opt := &gitlab.EditProjectOptions{}
	if got, want := p.SharedRunnersEnabled, false; got != want {
		log.Printf("%s: disabling shared runners", p.PathWithNamespace)
		opt.SharedRunnersEnabled = gitlab.Bool(false)
		change = true
	}
	// requires https://github.com/xanzy/go-gitlab/pull/306
	if got, want := p.CIConfigPath, ciFileName; got == nil || *got != want {
		log.Printf("%s: setting ci config path to %q", p.PathWithNamespace, want)
		opt.CIConfigPath = gitlab.String(ciFileName)
		change = true
	}
	if !change {
		return nil
	}

	// TODO(stapelberg): remove the following line once
	// https://gitlab.com/gitlab-org/gitlab-ce/issues/42509 is fixed:
	opt.SharedRunnersEnabled = gitlab.Bool(false)

	if _, _, err := salsa.Projects.EditProject(p.ID, opt); err != nil {
		return err
	}
	return nil
}

func FixProjectRunners(p *gitlab.Project) error {
	// requires https://github.com/xanzy/go-gitlab/pull/307
	runners, _, err := salsa.Runners.ListProjectRunners(p.ID, nil)
	if err != nil {
		return err
	}
	found := false
	for _, r := range runners {
		if r.ID == runnerID {
			found = true
			break
		}
	}
	if found {
		return nil
	}
	log.Printf("%s: enabling pkg-go CI runner", p.PathWithNamespace)
	if _, _, err := salsa.Runners.EnableProjectRunner(p.ID, &gitlab.EnableProjectRunnerOptions{
		RunnerID: runnerID,
	}); err != nil {
		return err
	}
	return nil
}

func WriteGitlabCiYml(p *gitlab.Project) error {
	branches, _, err := salsa.Branches.ListBranches(p.ID, &gitlab.ListBranchesOptions{})
	if err != nil {
		return err
	}
	for _, branch := range branches {
		if branch.Name != "master" && !strings.HasPrefix(branch.Name, "debian/") {
			continue
		}

		b, _, err := salsa.RepositoryFiles.GetRawFile(p.ID, ciFileName, &gitlab.GetRawFileOptions{
			Ref: gitlab.String(branch.Name),
		})
		if err != nil {
			if er, ok := err.(*gitlab.ErrorResponse); !ok || er.Response.StatusCode != 404 {
				return err
			}
			log.Printf("%s: creating debian/gitlab-ci.yml in branch %s", p.PathWithNamespace, branch.Name)
			if _, _, err := salsa.RepositoryFiles.CreateFile(p.ID, ciFileName, &gitlab.CreateFileOptions{
				Branch:        gitlab.String(branch.Name),
				Content:       gitlab.String(gitlabciymlTmpl),
				CommitMessage: gitlab.String(commitMsg),
			}); err != nil {
				return err
			}
			continue
		}
		if string(b) == gitlabciymlTmpl {
			continue // up to date
		}
		log.Printf("%s: updating debian/gitlab-ci.yml in branch %s", p.PathWithNamespace, branch.Name)
		if _, _, err := salsa.RepositoryFiles.UpdateFile(p.ID, ciFileName, &gitlab.UpdateFileOptions{
			Branch:        gitlab.String(branch.Name),
			Content:       gitlab.String(gitlabciymlTmpl),
			CommitMessage: gitlab.String(commitMsg),
		}); err != nil {
			return err
		}
	}
	return nil
}

func maybeAddTagpending(p *gitlab.Project, hooks []*gitlab.ProjectHook) error {
	for _, hook := range hooks {
		if strings.HasPrefix(hook.URL, "https://webhook.salsa.debian.org/tagpending/") {
			return nil // hook already set up
		}
	}
	_, _, err := salsa.Projects.AddProjectHook(p.ID, &gitlab.AddProjectHookOptions{
		URL:        gitlab.String("https://webhook.salsa.debian.org/tagpending/" + p.Name),
		PushEvents: gitlab.Bool(true),
	})

	return err
}

func maybeAddKGB(p *gitlab.Project, hooks []*gitlab.ProjectHook) error {
	const kgbBase = "http://kgb.debian.net:9418/webhook/"
	u, _ := url.Parse(kgbBase)
	q := u.Query()
	q.Set("channel", "#debian-golang")
	u.RawQuery = q.Encode()

	for _, hook := range hooks {
		if strings.HasPrefix(hook.URL, kgbBase) {
			return nil // hook already set up
		}
	}
	_, _, err := salsa.Projects.AddProjectHook(p.ID, &gitlab.AddProjectHookOptions{
		URL:        gitlab.String(u.String()),
		PushEvents: gitlab.Bool(true),
	})

	return err
}

func FixWebhooks(p *gitlab.Project) error {
	hooks, _, err := salsa.Projects.ListProjectHooks(p.ID, &gitlab.ListProjectHooksOptions{})
	if err != nil {
		return err
	}

	if err := maybeAddTagpending(p, hooks); err != nil {
		return err
	}

	if err := maybeAddKGB(p, hooks); err != nil {
		return err
	}

	return nil
}

func ProtectBranches(p *gitlab.Project) error {
	branches, _, err := salsa.ProtectedBranches.ListProtectedBranches(p.ID, &gitlab.ListProtectedBranchesOptions{})
	if err != nil {
		return err
	}
	toProtect := map[string]bool{
		"master":       true,
		"debian/*":     true,
		"upstream":     true,
		"upstream/*":   true,
		"pristine-tar": true,
	}
	for _, branch := range branches {
		delete(toProtect, branch.Name)
	}
	for branch := range toProtect {
		_, _, err := salsa.ProtectedBranches.ProtectRepositoryBranches(p.ID, &gitlab.ProtectRepositoryBranchesOptions{
			Name:             gitlab.String(branch),
			PushAccessLevel:  gitlab.AccessLevel(gitlab.DeveloperPermissions),
			MergeAccessLevel: gitlab.AccessLevel(gitlab.DeveloperPermissions),
		})
		if err != nil {
			return err
		}
	}
	return nil
}
